# Contributing to tutorials for CoderDojo

## I just have a question!

Ask a question by creating a new issue with a short title. Please use the label `question`.

## How Can I Contribute?

Search the issue tracker for similar entries before submitting your own.

### Reporting Bugs

Make an issue with the label `bug`.

### Suggesting Enhancements

Make an issue with the label `enhancement`.

### Your First Code Contribution

You can start by looking through these `beginner` and `help-wanted` issues:

* [Beginner issues][beginner] - issues which should only require a few lines of code, and a test or two.
* [Help wanted issues][help-wanted] - issues which should be a bit more involved than `beginner` issues.

#### Local development

To build and test the pages locally, run:

```sh
git clone https://gitlab.com/sharethewisdom/html.git
cd html
npm i reveal.js/
npm run serve
```
