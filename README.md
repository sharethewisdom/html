[![pipeline status](https://gitlab.com/sharethewisdom/html/badges/main/pipeline.svg)](https://gitlab.com/sharethewisdom/html/-/commits/main)

# HTML

Here are few HTML, CSS and Javascript tutorials for CoderDojo (using reveal.js).

Gitlab's continuous integration tool will build this project, and update the pages served at https://sharethewisdom.gitlab.io/html/ when it receives changes to the "main" branch.

## Development

To build and test the pages locally, run:

```sh
git clone https://gitlab.com/sharethewisdom/html.git
cd html
npm i reveal.js/
npm run serve
```
